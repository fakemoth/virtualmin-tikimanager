#!/usr/bin/perl
use strict;
use warnings;

our (%access, %text, %in);
our $module_name;

require './virtualmin-tikimanager-lib.pl';
&ReadParse();

# check if user can access the page
# &can_domain($in{'dom'}) || &error($text{'contact_ecannot'});

my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}

if (! $d) {
  &error("Domain not found.");
}

my $branch = $in{'branch'};
my (%info) = &tikimanager_tiki_info($d);

# Page title, must be first UI thing
&ui_print_header(
  'at ' . '<a href="https://' . $d->{'dom'} .'" target="_blank">https://' . $d->{'dom'} .'</a>',
  'Upgrading Tiki', "", undef, 1, 1
);

my %branches = map { $_ => 1 } &tikimanager_get_installable_branches($d);
if (! exists($branches{$branch}) || ! $info{'instance_id'}) {
  &error("Domain is not installable.");
}

sub print_ln () {
  my ($str) = @_;
  $str =~ s/\s*$//;
  $str =~ s/</&lt;/;
  $str =~ s/>/&gt;/;
  print "$str\n";
}

&$virtual_server::first_print("Upgrading Tiki using Tiki Manager..");
print '<pre style="white-space: pre-wrap; background-color: black; color: white;">' . "\n";
&tikimanager_tiki_upgrade($d, $info{'instance_id'}, $branch, \&print_ln);
print '</pre>' . "\n";
&$virtual_server::second_print(".. done");

&ui_print_footer(
  $d ? &virtual_server::domain_footer_link($d) : ( ),
  "index.cgi?dom=$in{'dom'}",
  $text{'index_the_information_page'}
);
