#!/usr/bin/perl
use strict;
use warnings;

our (%access, %text, %in, %userconfig);
our ($module_name, $path);

require './virtualmin-tikimanager-lib.pl';
&ReadParse();

my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}
if (! $d) {
  &error("Domain not found.");
}

my $config = &tikimanager_tiki_get_config($d) || &error("Can't open local.php");
my $filename = 'tiki.ini';
my $cwd = "$d->{'public_html_path'}/db";
my $path = "$d->{'public_html_path'}/db";
my $file = "$cwd/$filename";
my $identifier = 'tikiwiki';

my $data = $in{'data'};
$data =~ s/\r\n/\n/g;

if ($data =~ m/^ *\[([^\]]+) *\]/) {
  $identifier = $1;
}
else {
  $data = "[$identifier]\n$data";
}

my $SAVE;
&open_tempfile($SAVE, ">$file") || &error("Can't save tiki ini file: " . &html_escape("$!"));
&print_tempfile($SAVE, $data);
&close_tempfile($SAVE);

$config->{'system_configuration_file'} = 'db/tiki.ini';
$config->{'system_configuration_identifier'} = $identifier;

&tikimanager_tiki_save_config($d, $config) || &error("local.php was not saved or corrupted");

if ($in{'save_close'}) {
  &redirect("index.cgi?dom=$d->{'id'}");
} else {
  &redirect("tiki-edit-ini.cgi?dom=$d->{'id'}");
}