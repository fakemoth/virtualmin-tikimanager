#!/usr/bin/perl
use strict;
use warnings;

our (%access, %text, %in, %userconfig);
our ($module_name, $path);

require './virtualmin-tikimanager-lib.pl';
&ReadParse();

my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}
if (! $d) {
  &error("Domain not found.");
}


my $filename = 'tiki.ini';
my $cwd = "$d->{'public_html_path'}/db";
my $path = "$d->{'public_html_path'}/db";
my $file = "$cwd/$filename";

my $data = "";
if (-e "$file") {
  $data = &ui_read_file_contents_limit({ 'file', $file });
}

# Page title, must be first UI thing
&ui_print_header(
  'for ' . '<a href="https://' . $d->{'dom'} .'" target="_blank">https://' . $d->{'dom'} .'</a>',
  "Editing $filename", "", undef, 1, 1
);

print &ui_form_start("tiki-save-ini.cgi", "post", undef, ('data-encoding="utf-8"'));
print &ui_table_start(&html_escape("$path/$filename"), undef, 1);
print &ui_hidden("dom", $d->{'id'}), "\n";
print &ui_textarea("data", $data, 20, 80, undef, undef, "style='width: 100%' id='data'");
print &ui_hidden("path", $path);
print &ui_table_row("", "", 2);
print &ui_table_row('Documentation:', '<a href="https://doc.tiki.org/System-Configuration"'
      . ' target="_blank">'
      . ' https://doc.tiki.org/System-Configuration'
      . '</a>'
);
print &ui_table_row("", "", 2);
print &ui_table_end();
print &ui_form_end([['save', 'Save'], ['save_close', 'Save and close']]);

&ui_print_footer(
  $d ? &virtual_server::domain_footer_link($d) : ( ),
  "index.cgi?dom=$in{'dom'}",
  $text{'index_the_information_page'}
);
