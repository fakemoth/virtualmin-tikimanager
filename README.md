This script permits to manage Tiki instances within Virtualmin Virtual Servers, leveraging Tiki Manager.


As of 2021-10-18, it is for developers and sysadmins only, and it will be ready for power users in a few weeks: https://gitlab.com/wikisuite/virtualmin-installer/-/issues/7


